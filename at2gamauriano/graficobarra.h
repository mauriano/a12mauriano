#ifndef GRAFICOBARRA_H
#define GRAFICOBARRA_H

#include<grafico.h>

class GraficoBarra: public Grafico
{
public:
    GraficoBarra();
    void plot(QCustomPlot* customPlot);
};

#endif // GRAFICOBARRA_H
