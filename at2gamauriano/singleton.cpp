#include "singleton.h"

Singleton* Singleton::instance = 0;
QList<Dados*> Singleton::dados;

Singleton* Singleton::getInstance(){
    if(!instance){
        instance = new Singleton();
        carregaDados();
    }
    return instance;
}

Singleton::Singleton()
{

}

void Singleton::carregaDados(){

    dados.push_back(new Dados("2020-09-30 13:09:00", 61409.31821, 61550, 61100.71));
    dados.push_back(new Dados("2020-09-30 14:09:00", 61398.61217, 61550, 61118.35));
    dados.push_back(new Dados("2020-09-30 15:09:00", 60916.95202, 60921, 60901));
    dados.push_back(new Dados("2020-09-30 16:09:00", 60837.22411, 60837.7, 60837.69));
    dados.push_back(new Dados("2020-09-30 17:09:00", 61068.07596, 61152.21, 60838.69));
    dados.push_back(new Dados("2020-09-30 18:09:00", 61013.57756, 61130, 60842.2));
    dados.push_back(new Dados("2020-09-30 19:09:00", 60819.365, 60842.22, 60842.2));
    dados.push_back(new Dados("2020-09-30 20:09:00", 60929.2, 61149.98, 60837.69));
    dados.push_back(new Dados("2020-09-30 21:09:00", 61055.67336, 61141.74, 60854.55));
    dados.push_back(new Dados("2020-09-30 22:09:00", 60962.98835, 61000.03, 60870.05));
    dados.push_back(new Dados("2020-09-30 23:09:00", 60949.33871, 61123.64, 60850.16));
    dados.push_back(new Dados("2020-09-30 00:09:00", 61015.82626, 61100.62, 60855));
    dados.push_back(new Dados("2020-10-01 01:10:00", 60964.68886, 61007.78, 60901));
    dados.push_back(new Dados("2020-10-01 02:10:00", 60890.72782, 61093.98, 60900));
    dados.push_back(new Dados("2020-10-01 03:10:00", 61027.79199, 61122.78, 60853));
    dados.push_back(new Dados("2020-10-01 04:10:00", 60845.2764, 60855.11, 60842));
    dados.push_back(new Dados("2020-10-01 05:10:00", 60960.17864, 61065.87, 60970));
    dados.push_back(new Dados("2020-10-01 05:10:00", 61002.51636, 61065.91, 60970));
    dados.push_back(new Dados("2020-10-01 05:10:00", 61112.14575, 61300, 61065.54));
    dados.push_back(new Dados("2020-10-01 06:10:00", 61039.36101, 61300, 61000.02));
    dados.push_back(new Dados("2020-10-01 06:10:00", 61058.30425, 61299.93, 61000.02));
    dados.push_back(new Dados("2020-10-01 06:10:00", 61221.63355, 61400, 61000.81));
    dados.push_back(new Dados("2020-10-01 06:10:00", 61007.40889, 61011.74, 61006.22));
    dados.push_back(new Dados("2020-10-01 07:10:00", 61020.37792, 61316.47, 61002.57));
    dados.push_back(new Dados("2020-10-01 08:10:00", 61071.48697, 61299.87, 61004.15));
    dados.push_back(new Dados("2020-10-01 09:10:00", 61110.03793, 61298.97, 60950));
    dados.push_back(new Dados("2020-10-01 10:10:00", 61545.5333, 61735.65, 61310.61));
    dados.push_back(new Dados("2020-10-01 11:10:00", 61447.58675, 61597.9, 61310.61));
    dados.push_back(new Dados("2020-10-01 12:10:00", 61492.67617, 61697.99, 61355.02));
    dados.push_back(new Dados("2020-10-01 13:10:00", 61041.4611, 61350.01, 60800));
    //30
}

QList<Dados*> Singleton::getDados(){
    return dados;
}
