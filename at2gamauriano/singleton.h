#ifndef SINGLETON_H
#define SINGLETON_H

#include<QList>
#include<QVector>
#include<dados.h>

class Singleton
{
public:
    Singleton();
    static QList<Dados*> getDados();
    static Singleton* getInstance();

private:
    static QList<Dados*> dados;
    static Singleton* instance;
    static void carregaDados();

};

#endif // SINGLETON_H
