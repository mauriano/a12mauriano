#include "grafico.h"



Grafico::Grafico()
{
}


QVector<double>
Grafico::getX(QList<Dados*> dados)
{
    QVector<double> x(dados.length());
    for(int i = 0; i < dados.length(); i++)
    {
        x[i] = i;
    }
    return x;
}

QVector<double>
Grafico::getWeightedX(QList<Dados*> dados)
{
    return getX(dados);
}

QVector<double>
Grafico::getWeightedY(QList<Dados*> dados)
{
    QVector<double> y(dados.length());
    for(int i = 0; i < dados.length(); i++)
    {
        y[i] = dados.at(i)->getWeighted();
    }
    return y;
}

QVector<double>
Grafico::getLowX(QList<Dados*> dados)
{
    return getX(dados);
}

QVector<double>
Grafico::getLowY(QList<Dados*> dados)
{
    QVector<double> y(dados.length());
    for(int i = 0; i < dados.length(); i++)
    {
        y[i] = dados.at(i)->getLow();
    }
    return y;
}

QVector<double>
Grafico::getHighX(QList<Dados*> dados)
{
    return getX(dados);
}

QVector<double>
Grafico::getHighY(QList<Dados*> dados)
{
    QVector<double> y(dados.length());
    for(int i = 0; i < dados.length(); i++)
    {
        y[i] = dados.at(i)->getHigh();
    }
    return y;
}

unsigned int
Grafico::getLength(QList<Dados *> dados)
{
    return dados.length();
}

double
Grafico::getAmplitude(QList<Dados*> dados)
{
    double min = 10000000000000, max = -10000000000000;
    double w, h, l;
    QVector<double> y(dados.length());
    for(int i = 0; i < dados.length(); i++)
    {
        w = dados.at(i)->getWeighted();
        l = dados.at(i)->getLow();
        h = dados.at(i)->getHigh();

        if(min > w) { min = w; }
        if(min > h) { min = h; }
        if(min > l) { min = l; }

        if(max < w) { max = w; }
        if(max < h) { max = h; }
        if(max < l) { max = l; }
    }
    return max - min;
}

double
Grafico::getMin(QList<Dados*> dados)
{
    double min = 10000000000000;
    double w, h, l;
    QVector<double> y(dados.length());
    for(int i = 0; i < dados.length(); i++)
    {
        w = dados.at(i)->getWeighted();
        l = dados.at(i)->getLow();
        h = dados.at(i)->getHigh();

        if(min > w) { min = w; }
        if(min > h) { min = h; }
        if(min > l) { min = l; }
    }
    return min;
}

double
Grafico::getMax(QList<Dados*> dados)
{
    double max = -10000000000000;
    double w, h, l;
    QVector<double> y(dados.length());
    for(int i = 0; i < dados.length(); i++)
    {
        w = dados.at(i)->getWeighted();
        l = dados.at(i)->getLow();
        h = dados.at(i)->getHigh();

        if(max < w) { max = w; }
        if(max < h) { max = h; }
        if(max < l) { max = l; }
    }
    return max;
}


QVector<QString>
Grafico::getDatas(QList<Dados*> dados)
{
    QVector<QString> str(dados.length());
    for(int i = 0; i < dados.length(); i++)
    {
        str[i] = dados.at(i)->getData().toString();
    }
    return str;
}
