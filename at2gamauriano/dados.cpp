#include "dados.h"

Dados::Dados()
{

}

Dados::Dados(float w, float h, float l){
    setLow(l);
    setHigh(h);
    setWeighted(w);
}

Dados::Dados(QString d, float w, float h, float l){
    setData(QDateTime::fromString(d, "yyyy-MM-dd HH:mm:ss"));
    setLow(l);
    setHigh(h);
    setWeighted(w);
}

void Dados:: setLow(float l)
{
    low = l;
}

void Dados:: setData(QDateTime d){
    data = d;
}


void Dados:: setHigh(float h){
    high = h;
}

void Dados:: setWeighted(float w){
    weighted = w;
}

float Dados:: getLow(){
    return low;
}

QDateTime Dados:: getData(){
    return data;
}

float Dados:: getHigh(){
    return high;
}

float Dados:: getWeighted(){
    return weighted;
}
