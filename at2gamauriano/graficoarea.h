#ifndef GRAFICOAREA_H
#define GRAFICOAREA_H

#include<grafico.h>

class GraficoArea: public Grafico
{
public:
    GraficoArea();
    void plot(QCustomPlot* customPlot);
};

#endif // GRAFICOAREA_H
