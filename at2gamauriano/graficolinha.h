#ifndef GRAFICOLINHA_H
#define GRAFICOLINHA_H

#include<grafico.h>

class GraficoLinha: public Grafico
{
public:
    GraficoLinha();
    void plot(QCustomPlot* customPlot);
};

#endif // GRAFICOLINHA_H
