#include "graficobarra.h"

GraficoBarra::GraficoBarra()
{

}

void GraficoBarra::plot(QCustomPlot* customPlot){

    Singleton* l = Singleton::getInstance();
    QList<Dados*> dados = l->getDados();

    QVector<double> wx = this->getWeightedX(dados);
    QVector<double> wy = this->getWeightedY(dados);
    QVector<double> hx = this->getLowX(dados);
    QVector<double> hy = this->getLowY(dados);
    QVector<double> lx = this->getHighX(dados);
    QVector<double> ly = this->getHighY(dados);

    // set dark background gradient:
    QLinearGradient gradient(0, 0, 0, 400);
    gradient.setColorAt(0, QColor(90, 90, 90));
    gradient.setColorAt(0.38, QColor(105, 105, 105));
    gradient.setColorAt(1, QColor(70, 70, 70));
    customPlot->setBackground(QBrush(gradient));

    // create empty bar chart objects:
    QCPBars *weighted = new QCPBars(customPlot->xAxis, customPlot->yAxis);
    QCPBars *high = new QCPBars(customPlot->xAxis, customPlot->yAxis);
    QCPBars *low = new QCPBars(customPlot->xAxis, customPlot->yAxis);
    weighted->setAntialiased(false); // gives more crisp, pixel aligned bar borders
    high->setAntialiased(false);
    low->setAntialiased(false);

    weighted->setStackingGap(1);
    high->setStackingGap(1);
    low->setStackingGap(1);

    // set names and colors:
    weighted->setName("Weighted");
    weighted->setPen(QPen(QColor(111, 9, 176).lighter(170)));
    weighted->setBrush(QColor(111, 9, 176));
    high->setName("High");
    high->setPen(QPen(QColor(250, 170, 20).lighter(150)));
    high->setBrush(QColor(250, 170, 20));
    low->setName("Low");
    low->setPen(QPen(QColor(0, 168, 140).lighter(130)));
    low->setBrush(QColor(0, 168, 140));
    // stack bars on top of each other:
    //nuclear->moveAbove(fossil);
    //regen->moveAbove(nuclear);

    // prepare x axis with country labels:



    QVector<double> ticks;
    QVector<QString> labels;
    //ticks << 1 << 2 << 3 << 4;
    //labels << "2020-09-30 13:09:00" << "2020-09-30 24:09:00" << "2020-10-01 01:10:00" << "2020-10-01 13:10:00";
    ticks = this->getLowX(dados);
    labels = this->getDatas(dados);
    QSharedPointer<QCPAxisTickerText> textTicker(new QCPAxisTickerText);
    textTicker->addTicks(ticks, labels);
    customPlot->xAxis->setTicker(textTicker);
    customPlot->xAxis->setTickLabelRotation(60);
    customPlot->xAxis->setSubTicks(false);
    customPlot->xAxis->setTickLength(0, 4);
    customPlot->xAxis->setRange(0, this->getLength(dados));
    customPlot->xAxis->setBasePen(QPen(Qt::white));
    customPlot->xAxis->setTickPen(QPen(Qt::white));
    customPlot->xAxis->grid()->setVisible(true);
    customPlot->xAxis->grid()->setPen(QPen(QColor(130, 130, 130), 0, Qt::DotLine));
    customPlot->xAxis->setTickLabelColor(Qt::white);
    customPlot->xAxis->setLabelColor(Qt::white);

    // prepare y axis:
    customPlot->yAxis->setRange(this->getMin(dados), this->getMax(dados));
    customPlot->yAxis->setPadding(5); // a bit more space to the left border
    customPlot->yAxis->setLabel("Valores");
    customPlot->yAxis->setBasePen(QPen(Qt::white));
    customPlot->yAxis->setTickPen(QPen(Qt::white));
    customPlot->yAxis->setSubTickPen(QPen(Qt::white));
    customPlot->yAxis->grid()->setSubGridVisible(true);
    customPlot->yAxis->setTickLabelColor(Qt::white);
    customPlot->yAxis->setLabelColor(Qt::white);
    customPlot->yAxis->grid()->setPen(QPen(QColor(130, 130, 130), 0, Qt::SolidLine));
    customPlot->yAxis->grid()->setSubGridPen(QPen(QColor(130, 130, 130), 0, Qt::DotLine));


    weighted->setData(wx, wy);
    high->setData(hx, hy);
    low->setData(lx, ly);

    // setup legend:
    customPlot->legend->setVisible(false);
    customPlot->axisRect()->insetLayout()->setInsetAlignment(0, Qt::AlignBottom|Qt::AlignBottom);
    customPlot->legend->setBrush(QColor(255, 255, 255, 100));
    customPlot->legend->setBorderPen(Qt::NoPen);
    QFont legendFont;
    legendFont.setPointSize(10);
    customPlot->legend->setFont(legendFont);
    customPlot->setInteractions(QCP::iRangeDrag | QCP::iRangeZoom);

}
