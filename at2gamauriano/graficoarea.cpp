#include "graficoarea.h"

GraficoArea::GraficoArea()
{

}

void GraficoArea::plot(QCustomPlot* customPlot){

    Singleton* l = Singleton::getInstance();
    QList<Dados*> dados = l->getDados();

    QVector<double> wx = this->getWeightedX(dados);
    QVector<double> wy = this->getWeightedY(dados);
    QVector<double> hx = this->getLowX(dados);
    QVector<double> hy = this->getLowY(dados);
    QVector<double> lx = this->getHighX(dados);
    QVector<double> ly = this->getHighY(dados);

    // add two new graphs and set their look:
    customPlot->addGraph();
    customPlot->graph(0)->setPen(QPen(Qt::yellow));
    customPlot->graph(0)->setBrush(QBrush(QColor(0, 0, 255, 20))); // first graph will be filled with translucent blue
    customPlot->addGraph();
    customPlot->graph(1)->setPen(QPen(Qt::red)); // line color red for second graph
    customPlot->addGraph();
    customPlot->graph(2)->setPen(QPen(Qt::blue)); //
    customPlot->graph(2)->setBrush(QBrush(QColor(0, 0, 255, 20)));

    // configure right and top axis to show ticks but no labels:
    // (see QCPAxisRect::setupFullAxesBox for a quicker method to do this)
    customPlot->xAxis2->setVisible(true);
    customPlot->xAxis2->setTickLabels(false);
    customPlot->yAxis2->setVisible(true);
    customPlot->yAxis2->setTickLabels(false);
    // make left and bottom axes always transfer their ranges to right and top axes:
    //connect(customPlot->xAxis, SIGNAL(rangeChanged(QCPRange)), customPlot->xAxis2, SLOT(setRange(QCPRange)));
    //connect(customPlot->yAxis, SIGNAL(rangeChanged(QCPRange)), customPlot->yAxis2, SLOT(setRange(QCPRange)));
    // pass data points to graphs:
    customPlot->graph(0)->setData(wx, wy);
    customPlot->graph(1)->setData(hx, hy);
    customPlot->graph(2)->setData(lx, ly);
    // let the ranges scale themselves so graph 0 fits perfectly in the visible area:
    customPlot->graph(0)->rescaleAxes();
    // same thing for graph 1, but only enlarge ranges (in case graph 1 is smaller than graph 0):
    customPlot->graph(1)->rescaleAxes(true);
    customPlot->graph(2)->rescaleAxes(true);
    // Note: we could have also just called customPlot->rescaleAxes(); instead
    // Allow user to drag axis ranges with mouse, zoom with mouse wheel and select graphs by clicking:
    customPlot->setInteractions(QCP::iRangeDrag | QCP::iRangeZoom | QCP::iSelectPlottables);
}
