#include "mainwindow.h"
#include "ui_mainwindow.h"

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    ui->setupUi(this);
    Grafico *graficoLinha = new GraficoLinha();
    graficoLinha->plot(ui->grafico1);
    Grafico *graficoBarra = new GraficoBarra();
    graficoBarra->plot(ui->grafico2);
    Grafico *graficoData = new GraficoArea();
    graficoData->plot(ui->grafico3);
}

MainWindow::~MainWindow()
{
    delete ui;
}

