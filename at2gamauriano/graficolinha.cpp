#include "graficolinha.h"

GraficoLinha::GraficoLinha()
{

}

void GraficoLinha::plot(QCustomPlot* customPlot){

    Singleton* l = Singleton::getInstance();
    QList<Dados*> dados = l->getDados();

    QVector<double> wx = this->getWeightedX(dados);
    QVector<double> wy = this->getWeightedY(dados);
    QVector<double> hx = this->getLowX(dados);
    QVector<double> hy = this->getLowY(dados);
    QVector<double> lx = this->getHighX(dados);
    QVector<double> ly = this->getHighY(dados);

    customPlot->addGraph();
    customPlot->graph(0)->setData(wx, wy);
    customPlot->addGraph();
    customPlot->graph(0)->setPen(QPen(Qt::yellow));
    customPlot->graph(1)->setData(hx, hy);
    customPlot->addGraph();
    customPlot->graph(1)->setPen(QPen(Qt::red));
    customPlot->graph(2)->setData(lx, ly);
    customPlot->graph(2)->setPen(QPen(Qt::blue));

    customPlot->xAxis->setLabel("Data");
    customPlot->yAxis->setLabel("Valores");
    customPlot->xAxis->setRange(0, this->getLength(dados));
    customPlot->yAxis->setRange(this->getMin(dados), this->getMax(dados));
    customPlot->replot();

}

