#ifndef DADOS_H
#define DADOS_H

#include<QDateTime>

class Dados
{
public:
    Dados();
    Dados(float w, float h, float l);
    Dados(QString d, float w, float h, float l);
    void setWeighted(float w);
    void setHigh(float h);
    void setLow(float l);
    void setData(QDateTime d);
    QDateTime getData();
    float getWeighted();
    float getHigh();
    float getLow();

private:
    QDateTime data;
    float weighted;
    float high;
    float low;
};

#endif // DADOS_H
