#ifndef GRAFICO_H
#define GRAFICO_H

#include<qcustomplot.h>
#include<singleton.h>
#include<dados.h>
#include<QList>

class Grafico
{
public:
    virtual void plot(QCustomPlot* customPlot) = 0;
    Grafico();
    QVector<double> getWeightedX(QList<Dados*> dados);
    QVector<double> getWeightedY(QList<Dados*> dados);
    QVector<double> getLowX(QList<Dados*> dados);
    QVector<double> getLowY(QList<Dados*> dados);
    QVector<double> getHighX(QList<Dados*> dados);
    QVector<double> getHighY(QList<Dados*> dados);
    unsigned int getLength(QList<Dados*> dados);
    double getAmplitude(QList<Dados*> dados);
    double getMin(QList<Dados*> dados);
    double getMax(QList<Dados*> dados);
    QVector<QString> getDatas(QList<Dados*> dados);

private:
    QVector<double> getX(QList<Dados*> pDados);
};

#endif // GRAFICO_H
